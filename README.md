# personal_page

Online Projects
http://listMe.xyz (03/17)
Full stack CRUD todo list. (Node, knex, psql, JQuery, handlebars, CSS)
http://writeIt.pro (02/17)
Full stack CRUD blog. wysiwyg editor, image, video upload. Responsive. (Node, psql, JavaScript, handlebars)
https://git.io/vMM8C (01/17)
CSS tutorial. Responsive (html, css)
https://git.io/vyfiO (01/17)
Spotify artists discography (html, css, javascript, promises, Ajax calls)
goo.gl/s9sHDS (11/16)
Image filtering console. node.js server, html, css, javascript (wip)
http://sportyTourist.com (10/17)
Sports news aggregator (html, css, javascript, jquery) (wip)
http://hangman.pro (08/16)
Game. Ajax call for random word for a game of hangman. (html, css, jquery)

Professional
Developer at Lenda.com(4/17 - )
PDF viewer app that allows the user to leave comments, interact with other users and collaborate.
Search QA Lead Editor at Yahoo Inc (3/13 – 02/16)
Lead the Algo Search Query Triage escalations team.
In charge escalations for relevance, triggering, ranking with Yahoo and Bing PMs and engineering teams.
Managed the Search results enhancement requests between the Yahoo Search and Microsoft’s Bing.
Lead the shopping search verticals QA team through query evaluations, use cases, debugging, escalations.
Project Coordinator / Leader at Google Inc (02/11 – 03/13)
Oversaw the mapping production evaluation for ~25 countries in four different launch cycles.
Coordinated the work of six Domain Leads for all the sections of the In-Operations processes.
Created and coordinated the team's internal website used for project goals, and team training.
Assisted ~30 Data Specialists with policy guidelines, issues resolution and client teams communication.
Search Editor at Yahoo Inc (11/09 – 02/11)
Front page content editing for Yahoo! News, Finance, Sports, Movies, TV, trending results.
Query interpretation and classification, user Intent analysis, timelessness, ambiguity.
Other employment at various positions (8/98 - 7/09)

Academic
Full Stack Immersive Program at Galvanize (9/16 - 2/17)
Masters in Sports Management at USF (8/02 - 5/04)
BSc Physical Ed. at Democritus University (9/94 - 5/99)


Technical skills
Express, PostgreSQL, JavaScript (ES5, ES6), JQuery, REST, Knex.js, Ajax, Regex, Node.js, NPM, HTML, CSS, Vue.js

ADD LINKEDIN AND GITHUB LINKS
